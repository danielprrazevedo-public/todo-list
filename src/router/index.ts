import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import { getAuth } from 'firebase/auth';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: '',
      component: HomeView,
    },
    {
      path: '/home',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue'),
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import('../views/LoginView.vue'),
    },
    {
      path: '/new-user',
      name: 'NewUser',
      component: () => import('../views/NewUserView.vue'),
    },
  ],
});

router.beforeEach(async (to) => {
  const auth = getAuth();

  if (!['Login', 'NewUser'].includes(to.name as string)) {
    const user = await new Promise((resolve) => {
      auth.onAuthStateChanged(resolve);
    });
    if (!user) return { name: 'Login' };
  }
});

export default router;
